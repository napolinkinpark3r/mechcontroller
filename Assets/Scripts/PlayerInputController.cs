﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour
{

	private MechController mechController;

	// Use this for initialization
	void Start ()
	{
		mechController = gameObject.GetComponent<MechController> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		mechController.Hor = Input.GetAxis ("Horizontal");
		mechController.Vert = Input.GetAxis ("Vertical");
		mechController.HorView = Input.GetAxis ("Mouse X");
		mechController.VertView = Input.GetAxis ("Mouse Y");
	}
}
