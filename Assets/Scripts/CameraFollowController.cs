﻿using UnityEngine;
using System.Collections;

public class CameraFollowController : MonoBehaviour
{

	public Transform target;
	public float distance;
	public float height;
	public float heightDamping;
	public float rotationDamping;
	//
	//
	//
	private float wantedRotationAngle;
	private float wantedHeight;
	private float currentRotationAngle;
	private float currentHeight;
	private Quaternion currentRotation;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void LateUpdate ()
	{
		// Calculate the current rotation angles
		wantedRotationAngle = target.eulerAngles.y;	
		wantedHeight = target.position.y + height;
		
		currentRotationAngle = transform.eulerAngles.y;
		currentHeight = transform.position.y;
		
		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
		
		// Damp the height
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
		
		// Convert the angle into a rotation
		currentRotation = Quaternion.Euler (0, currentRotationAngle, 0);
//		Debug.Log (currentRotation + " " + currentRotation * Vector3.forward);
		
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		transform.position = new Vector3 (target.position.x, currentHeight, target.position.z);
		transform.position -= currentRotation * Vector3.forward * distance;
		
		// Always look at the target
//		transform.LookAt (target);
		transform.forward = target.forward;
	}
}
