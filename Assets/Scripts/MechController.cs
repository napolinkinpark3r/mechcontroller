﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class MechController : MonoBehaviour
{

	public GameObject armature;
	public GameObject turret;
	public GameObject bone;
//	public GameObject leftGun;
//	public GameObject rightGun;
	public GameObject crossHair;
	//
	//Armature movement speed and delay
	[Range(0,100)]
	public float
		armatureMoveSpeed;
	[Range(0f,1f)]
	public float
		armatureMoveDelay;
	//
	//Aramature rotation speed and delay
	[Range(0,100)]
	public float
		armatureRotSpeed;
	[Range(0f,1f)]
	public float
		armatureRotDelay;
	//
	//Turret rotation speed and delay
	[Range(0,100)]
	public int
		turretRotSpeed;
	[Range(0f,1f)]
	public float
		turretRotDelay;
	//
	//Crosshair movement and return speed
	[Range(0.0f, 100.0f)]
	public float
		crossHairMoveSpeed;
	[Range(0.0f, 100.0f)]
	public float
		crossHairReturnSpeed;
	[Range(0.0f, 1.0f)]
	public float
		crossHairSmoothRate;
	//
	//
	private Animator anim;
	//
	//
	private float halfScreenWidth;
	private float halfScreenHeight;
	//
	private float _hor;
	private float _vert;
	private float _horView;
	private float _vertView;
	//
	public float Hor {
		get {
			return _hor;
		}
		set { 
			_hor = value;
		}
	}

	public float Vert {
		get {
			return _vert;
		}
		set { 
			_vert = value;
		}
	}

	public float HorView {
		get {
			return _horView;
		}
		set { 
			_horView = value;
		}
	}

	public float VertView {
		get {
			return _vertView;
		}
		set { 
			_vertView = value;
		}
	}


	// Use this for initialization
	void Start ()
	{

		armature = gameObject.transform.FindChild ("Armature Pawn").gameObject;
		turret = gameObject.transform.FindChild ("Turret Pawn").gameObject;

		crossHair = GameObject.Find ("Crosshair");
	
		anim = gameObject.GetComponent<Animator> ();

		halfScreenWidth = Screen.width / 2;
		halfScreenHeight = Screen.height / 2;
	}

	void Update ()
	{
		//Move cursor on mouse drag
		crossHair.transform.localPosition += (Vector3.up * _vertView + Vector3.right * _horView) * crossHairMoveSpeed * crossHairSmoothRate;

		
		//Rotate turret with cursor local position around vertical axis
		Vector3 desiredTurretForward = turret.transform.forward + 
			turret.transform.right * (crossHair.transform.localPosition.x / halfScreenWidth);
		
		turret.transform.forward = Vector3.Lerp (turret.transform.forward, desiredTurretForward, Time.deltaTime * turretRotSpeed * turretRotDelay);

		turret.transform.position = bone.transform.position;
		turret.transform.rotation = new Quaternion (bone.transform.rotation.x,
		                                            turret.transform.rotation.y,
		                                            bone.transform.rotation.z,
		                                            turret.transform.rotation.w);

		//Rotate armature on Horizontal axis down
		if (Mathf.Abs (_hor) > 0f) {
			Vector3 desiredArmatureForward = (armature.transform.forward + armature.transform.right * _hor).normalized;
			armature.transform.forward = Vector3.Lerp (armature.transform.forward, desiredArmatureForward, Time.deltaTime * armatureRotSpeed * armatureRotDelay);
		}

		//Move mech on Vertical axis down
		if (Mathf.Abs (_vert) > 0f) {
			transform.position += armature.transform.forward * armatureMoveSpeed * armatureMoveDelay * _vert;
		} 

		//Send Vertical axis input as Speed to Mecanim
		anim.SetFloat ("Speed", Mathf.Abs (_vert));

		//PASTE YOUR SHOOTING MECHANICS
		//
		//
		//

		//Debugging maybeShooting direction
		Ray ray = new Ray (turret.transform.position, turret.transform.forward + 
			turret.transform.right * (crossHair.transform.localPosition.x / halfScreenWidth) + 
			turret.transform.up * (crossHair.transform.localPosition.y / halfScreenHeight));

		Debug.DrawRay (ray.origin, ray.direction * 10000, Color.blue);


		

	}

	void LateUpdate ()
	{
		//Interpolate cursor position to screen center after each frame
		crossHair.transform.localPosition = Vector3.Lerp (crossHair.transform.localPosition, Vector3.up * crossHair.transform.localPosition.y, Time.deltaTime * crossHairReturnSpeed * crossHairSmoothRate);
	}
}
